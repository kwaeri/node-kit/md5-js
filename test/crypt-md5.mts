/*
 * SPDX-PackageName: kwaeri/md5-js
 * SPDX-PackageVersion: 0.1.1
 * SPDX-PackageCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier:  Apache-2.0 WITH LLVM-exception OR MIT
 *
 * Testing a packaging of a JavaScript implementation of the RSA Data Security, Inc.
 * MD5 Message Digest Algorithm, as defined in RFC 1321.
 *
 * Version 2.2 See http://pajhome.org.uk/crypt/md5 for more info.
 */


'use strict'


// INCLUDES
import * as assert from 'assert';
import { crypt_md5 } from '../src/crypt-md5.mjs';


// DEFINES
const md5js = new crypt_md5();


// SANITY CHECK - Makes sure our tests are working proerly
describe(
   'PREREQUISITE',
   () => {

       describe(
           'Sanity Test(s)',
           () => {

               it(
                   'Should return true.',
                   async () => {
                       //const version = JSON.parse( ( await fs.readFile( path.join( './', 'package.json' ), { encoding: "utf8" } ) ) ).version;

                       //console.log( `VERSION: ${version}` );

                       return Promise.resolve(
                           assert.strictEqual( [1,2,3,4].indexOf(4), 3 )
                       );
                   }
               );

           }
       );

   }
);


// Primary tests for the module
describe(
   'JavaScript MD5 Functionality Test Suite',
   () => {

       describe(
           'hex_md5() Test',
           () => {

               it(
                   'Should return true, indicating that the expected hash was returned.',
                   () => {
                        assert.strictEqual( md5js.hex_md5( "password" ), "5f4dcc3b5aa765d61d8327deb882cf99" )
                   }
               );

           }
       );


   }
);
