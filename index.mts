/**
 * SPDX-PackageName: kwaeri/md5-js
 * SPDX-PackageVersion: 0.1.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES


// ESM WRAPPER
export {
    crypt_md5
} from './src/crypt-md5.mjs';
